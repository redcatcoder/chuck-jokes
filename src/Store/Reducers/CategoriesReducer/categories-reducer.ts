import {GET_CATEGORIES, SET_CHOSEN_CATEGORY} from "Store/constants";

import {ICategoryState} from "Store/Reducers/CategoriesReducer/types";

const initialState: ICategoryState = {
    categories: [],
    chosenCategory: 'random',
}

export const CategoriesReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case SET_CHOSEN_CATEGORY:
            return {...state, chosenCategory: action.chosenCategory}
        case GET_CATEGORIES:
            return {...state, categories: [...action.categories]}
        default:
            return {...state}
    }
}
