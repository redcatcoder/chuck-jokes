import {IJokeState} from "Store/Reducers/JokeReducer/types";
import {ICategoryState} from "Store/Reducers/CategoriesReducer/types";

export interface IReducers {
    categories?: ICategoryState,
    joke?: IJokeState
}
