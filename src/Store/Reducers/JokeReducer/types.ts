interface IJoke {
    categories: string[],
    createdAt: string,
    icon_url: string,
    id: string,
    updated_at: string,
    url: string,
    value: string
}

export interface IJokeState {
    joke: IJoke
}
