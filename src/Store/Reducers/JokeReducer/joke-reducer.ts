import { GET_JOKE } from 'Store/constants'

import { IJokeState } from "Store/Reducers/JokeReducer/types";

const initialState: IJokeState = {
    joke: null,
}

export const JokeReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case GET_JOKE:
            return {...state, joke: action.joke}
        default:
            return {...state}
    }
}
