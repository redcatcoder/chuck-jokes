import { combineReducers } from "redux";
import { CategoriesReducer } from "./CategoriesReducer/categories-reducer";
import { JokeReducer } from "./JokeReducer/joke-reducer";

export default combineReducers({
    categories: CategoriesReducer,
    joke: JokeReducer,
})
