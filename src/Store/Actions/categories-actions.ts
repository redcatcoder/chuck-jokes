import { Dispatch } from "redux";
import {GET_CATEGORIES, SET_CHOSEN_CATEGORY} from "Store/constants";
import { URL } from "Config";

const getCategoriesAction = (categories) => {
    return {
        type: GET_CATEGORIES,
        categories
    }
}

export const getCategories = () => {
    return async (dispatch: Dispatch) => {
        const response = await fetch(URL.BASE + URL.WAYS.JOKES + URL.WAYS.CATEGORIES, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })

        if (response.ok) {
            const data = await response.json();

            dispatch(getCategoriesAction(data))
        }
    }
}

const setChosenCategoryAction = (chosenCategory: string) => {
    return {
        type: SET_CHOSEN_CATEGORY,
        chosenCategory
    }
}

export const setChosenCategory = (chosenCategory: string) => {
    return (dispatch: Dispatch) => {
        dispatch(setChosenCategoryAction(chosenCategory))
    }
}
