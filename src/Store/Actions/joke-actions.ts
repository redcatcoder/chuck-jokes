import { Dispatch } from "redux";
import { GET_JOKE } from "Store/constants";
import { URL } from "Config";

const getJokeAction = (joke: string) => {
    return {
        type: GET_JOKE,
        joke
    }
}

export const getJoke = (category: string = 'random') => {
    return async (dispatch: Dispatch) => {
        let urlParamCategory = '';

        if (category !== 'random') {
            urlParamCategory = `?category=${category}`;
        }

        const response = await fetch(`${URL.BASE}${URL.WAYS.JOKES}/random${urlParamCategory}` , {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })

        if (response.ok) {
            const data = await response.json();

            dispatch(getJokeAction(data))
        }
    }
}