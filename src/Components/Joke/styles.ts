import {IStyles, theme} from "Config";

export const Styles: IStyles = {
    jokeContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    jokeText: {
        color: 'secondary.dark',
        textAlign: 'center',
        marginBottom: 3
    },
    jokeMoreButton: {
        backgroundColor: 'primary.main',
        padding: `${theme.spacing(2)} ${theme.spacing(6)}`,
        fontSize: '1rem',
        width: '50%',
        color: 'primary.contrastText',
        '&:hover': {
            backgroundColor: 'primary.dark',
        }
    }
}
