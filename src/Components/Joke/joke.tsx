import React, { memo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  Box,
  Button,
  Typography
} from "@mui/material";

import { Styles } from "./styles";

import { IReducers } from "Store/Reducers/types";

import { getJoke } from "Store/Actions/joke-actions";

export const Joke = memo(() => {
  const dispatch = useDispatch();

  const chosenCategory = useSelector((state: IReducers) => state.categories.chosenCategory)

  useEffect( () => {
    dispatch(getJoke(chosenCategory));
  }, [chosenCategory]);

  const joke = useSelector((state: IReducers) => state.joke.joke);

  const onMoreButtonClick = () => {
    dispatch(getJoke(chosenCategory));
  }

  return (
      <Box sx={Styles.jokeContainer}>
        <Typography variant="h5" sx={Styles.jokeText}>
        {
          joke &&
          joke.value
        }
        </Typography>
        <Button sx={Styles.jokeMoreButton} onClick={onMoreButtonClick}>More</Button>
      </Box>
  )
})
