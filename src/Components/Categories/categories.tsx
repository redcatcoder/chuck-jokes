import React, { memo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
    Box,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent
} from "@mui/material";

import {getCategories, setChosenCategory} from "Store/Actions/categories-actions";
import { IReducers } from "Store/Reducers/types";

export const Categories = memo(() => {
    const dispatch = useDispatch();

    useEffect( () => {
        dispatch(getCategories());
        }, []);

    const categories = ['random', ...useSelector((state: IReducers) => state.categories.categories)];

    const setFirstSymbolUppercase = (str: string) => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    const onChangeCategory = (event: SelectChangeEvent) => {
       dispatch(setChosenCategory(event.target.value))
    }

    return (
      <Box >
          <FormControl fullWidth>
              <InputLabel id="choose-category-select-label">Choose category</InputLabel>
              <Select
                  labelId="choose-category-select-label"
                  id="choose-category-select"
                  label="Choose category"
                  defaultValue="random"
                  onChange={onChangeCategory}
              >

                  {
                      categories && categories.length > 0 &&
                      categories.map( category => (
                          <MenuItem key={Math.random()} value={category}
                                    >{setFirstSymbolUppercase(category)}</MenuItem>
                        )
                      )
                  }
              </Select>
          </FormControl>
      </Box>
    );
})
