import { IStyles } from "Config";

export const Styles: IStyles = {
    appContainer: {
        width: '100vw',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    appInnerContainer: {
        width: '90%'
    }
}
