import React from 'react';
import { Provider } from "react-redux";
import {
    applyMiddleware,
    createStore
} from "redux";
import thunk from "redux-thunk";

import rootReducer from 'Store/Reducers/root-reducer'

import {
    ThemeProvider,
    Container,
    Grid
} from "@mui/material";

import { Styles } from "./styles";
import { theme } from "Config";

import { Categories } from "Components/Categories";
import { Joke } from "Components/Joke";


function App() {
  const store = createStore(rootReducer, applyMiddleware(thunk))

  return (
      <Provider store={store}>
          <ThemeProvider theme={theme}>
              <Container className="App" maxWidth={false} sx={Styles.appContainer}>
                  <Grid container spacing={2} sx={Styles.appInnerContainer}>
                      <Grid item xs={12}>
                          <Categories/>
                      </Grid>
                      <Grid item xs={12}>
                          <Joke/>
                      </Grid>
                  </Grid>
              </Container>
          </ThemeProvider>
      </Provider>
  );
}

export default App;
