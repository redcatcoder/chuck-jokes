import IStyles from "./Styles";
import theme from "./CustomTheme";
import { URL } from "./Api"

export { IStyles, theme, URL };
