interface IPseudo {
    [key: string]: string | number
}

export default interface IStyles {
    [key: string]: {
        [key: string]: string | number | IPseudo
    }
}
