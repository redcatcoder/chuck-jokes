import { IUrl } from "./types";

export const URL: IUrl = {
    BASE: 'https://api.chucknorris.io',
    WAYS: {
        JOKES: '/jokes',
        CATEGORIES: '/categories'
    }
}