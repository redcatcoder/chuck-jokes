export interface IUrl {
    BASE: string,
    WAYS: {
        [key: string]: string,
    }
}
