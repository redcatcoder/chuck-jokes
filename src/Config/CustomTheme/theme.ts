import { createTheme } from '@mui/material/styles';

const CustomThemeOptions = {
    palette: {
        primary: {
            light: '#ff8A2d',
            main: '#e67b29',
            dark: '#d57327',
            contrastText: '#ffffff',
        },
        secondary: {
            main: '#ffffff',
            dark: '#b0b0b0',
            contrastText: '#161616',
        },
        contrastThreshold: 3,
        tonalOffset: 0.2,
    },
    components: {
        MuiTypography: {
            defaultProps: {
                variantMapping: {
                    body1: 'span',
                    body2: 'p',
                },
            },
        },
    },
}

export const theme = createTheme(CustomThemeOptions);
